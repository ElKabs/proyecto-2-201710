package vos;

import java.util.Date;

import API.ILista;
import vos.VOGeneroPelicula;
import com.google.gson.annotations.SerializedName;


public class VOPelicula {
	/*
	 * nombre de la pel�cula
	 */
	@SerializedName("Title")
	private String nombre;
	
	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	@SerializedName("Released")
	private Date fechaLanzamineto;
	
	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	@SerializedName("Genre")
	private ILista<VOGeneroPelicula> generosAsociados;
	
	/*
	 * votos totales sobre la pel�cula
	 */
	@SerializedName("imdbVotes")
	 private int votostotales;  
	 
	 /*
	  * promedio anual de votos (hasta el 2016)
	  */
	 private int promedioAnualVotos; 
	
	@SerializedName("movieId")
	private String movieId;
	 
	 /*
	  * promedio IMBD
	  */
	 @SerializedName("imdbRating")
	 private double ratingIMBD;	
	 
	 @SerializedName("imdbData")
	 private ImdbData IMDBData;
	 
	 private double prioridad;
	 
	 private double promedioRating;
	 
	 private String pais;
	 
	 public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public double getPromedioRating() {
		return promedioRating;
	}

	public void setPromedioRating(double promedioRating) {
		this.promedioRating = promedioRating;
	}

	public VOPelicula() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamineto() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamineto(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ILista<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ILista<VOGeneroPelicula> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public int getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(int promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}
	 
	public String getMovieID()
	{
		return movieId;
	}
	
	public void setMovieID(String movieId)
	{
		this.movieId = movieId;
	}
	 
	public ImdbData getIMDBData()
	{
		return IMDBData;
	}
	
	public double getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(double prioridad) {
		this.prioridad = prioridad;
	}

	public void setIMDBData(ImdbData IMDBData)
	{
		this.IMDBData = IMDBData;
	}
	
}
