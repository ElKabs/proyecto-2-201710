package vos;

public class VORating {
	long userid;
	long movieid;
	double rating;
	long timestamp;
	double error;
	long idUser;

	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public long getMovieid() {
		return movieid;
	}
	public void setMovieid(long movieid) {
		this.movieid = movieid;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public double getError() {
		return error;
	}
	public void setError(double error) {
		this.error = error;
	}
	public void setIdUsuario(long usuarioID) {
		idUser = usuarioID;
	}
	public long getIdUsuario(){
		return idUser;
	}
	

}
