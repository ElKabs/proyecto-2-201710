package vos;

public class VOTag implements Comparable<VOTag>{
	private long idPelicula;
	private String tag;
	private long timestamp;
	private String conformismo;
	private long idUsuario;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getConformismo() {
		return conformismo;
	}
	public void setConformismo(String conformismo) {
		this.conformismo = conformismo;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VOTag o) {
		int respuesta = 0; 
		if (this.getTag().compareTo(o.getTag()) == 0) { respuesta =0; } 
		if (this.getTag().compareTo(o.getTag()) < 0) { respuesta = -1;} 
		if (this.getTag().compareTo(o.getTag()) > 0) {respuesta = 1;} 
		
		if (this.getTimestamp() == o.getTimestamp()) { respuesta = 0;} 
		if (this.getTimestamp() < o.getTimestamp()) { respuesta = -1;} 
		if (this.getTimestamp() > o.getTimestamp()) {respuesta = 1;} 
		
		return respuesta; 
	}
	public boolean equals(VOTag o){
		if (this.getTag().compareTo(o.getTag()) == 0 && this.getTimestamp() == o.getTimestamp()){
			return true; 
		}
		else 
			return false; 
	}
	
}
