package vos;

	public class VOTagSegmento {
		
		private String tag;
		private String Segmento;
		
		public String getSegmento() {
			return Segmento;
		}
		public void setSegmento(String segmento) {
			Segmento = segmento;
		}
		public void setTag(String tag) {
			this.tag = tag;
		}
		public String getTag() {
			return tag;
		}
}
