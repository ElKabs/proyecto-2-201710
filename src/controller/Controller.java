package controller;
import API.ISistemaRecomendacionPeliculas;
import logic.SistemaRecomendacionPeliculas;

public class Controller {
		private static ISistemaRecomendacionPeliculas manejador = new SistemaRecomendacionPeliculas();
		
		public static void r1(String rutaPeliculas)
		{
			manejador.cargarPeliculasSR(rutaPeliculas);
		}
		
		public static void r2(String rutaRatings)
		{
			manejador.cargarRatingsSR(rutaRatings);
		}
		
		public static void r3(String rutaTags)
		{
			manejador.cargarTagsSR(rutaTags);
		}
		
		public static void req1(int idUsuario, String ruta)
		{
			manejador.registrarSolicitudRecomendacion(idUsuario, ruta);
		}
}
