package logic;

import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import API.*;
import model.data_structures.*;
import vos.*;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {
	private final static String CONFORME="conforme";
	private final static String INCONFORME="inconforme";
	private final static String NEUTRAL="neutral";
	private final static String NOCLASIFICADO="no-clasificado";

	Gson gson = new Gson();
	int count = 0;
	private ListaEncadenada2<VOPelicula> misPeliculas;
	private ListaEncadenada2<VOGeneroPelicula> misGeneros;
	private ListaEncadenada2<VOReporteSegmento> misReportes;
	private ListaEncadenada2<VOTag> misTags;
	private ListaEncadenada2<VOTagSegmento> misTagsSegmento = new ListaEncadenada2<>();
	private ListaEncadenada2<VOUsuario> misUsuarios;
	private ListaEncadenada2<VOGeneroPelicula> misGeneros2;
	private ListaEncadenada2<VORating> misRatings;
	private MaxPQ<Solicitud> solicitudes = new MaxPQ<>(); 
	private EncadenamientoSeparadoGenero<String, RojoNegro<Date, VOPelicula>> misGeneros3 = new EncadenamientoSeparadoGenero<>(30);
	private RedBlack<Integer, SeparateChainingHashST<String, RedBlack<Double, Lista2<vos.VOPelicula, SeparateChainingHashST<Integer, vos.VOUsuarioPelicula>>>>> arbolOrdenAnio;
	private RedBlack<Integer,EncadenamientoSeparadoTH<String, RedBlack<Double,Lista2<VOPelicula,EncadenamientoSeparadoTH<Integer, VOUsuarioPelicula>> >>> arbolOrdenAnho;

	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		return new SistemaRecomendacionPeliculas();
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {

		boolean existe =false;
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(rutaPeliculas));
			VOPelicula[] resultado = gson.fromJson(br, VOPelicula[].class);
			misPeliculas = new ListaEncadenada2<>();
			for(int i = 0; i<resultado.length; i++)
			{
				VOPelicula actual = resultado[i];
				//Titulo
				actual.setNombre(actual.getIMDBData().getTitle());

				//Fecha ----- Revisar
				String fecha = actual.getIMDBData().getReleased(); //actual.getIMDBData().getReleased();
				SimpleDateFormat fechaf = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
				Date fFecha = null;
				if (fecha.equals("N/A")) 
				{
					fFecha = new Date();
				}
				else
				{
					fFecha = fechaf.parse(fecha);
				}
				actual.setFechaLanzamineto(fFecha);

				//Generos
				actual.setGenerosAsociados(agregarGeneros(actual.getIMDBData().getGenre()));
				//Votos Totales
				if(actual.getIMDBData().getImdbVotes().equals("N/A"))
				{
					actual.setVotostotales(0);
				}
				else if(!actual.getIMDBData().getImdbVotes().equals("N/A"))
				{
					String[] votosComa = actual.getIMDBData().getImdbVotes().split(",");
					String votosSinComa="";
					for(int j = 0; j<votosComa.length; j++)
					{
						votosSinComa+=votosComa[j];
					}
					int votos = Integer.parseInt(votosSinComa);
					actual.setVotostotales(votos);
				}

				//Promedio anual de votos
				String anio = actual.getIMDBData().getReleased();
				if(anio.split(" ").length==2)
				{
					int anio2 = Integer.parseInt(anio.split(" ")[2]);
					int hanPasado = 2017-anio2;
					int promedioT = actual.getVotostotales()/hanPasado;
					actual.setPromedioAnualVotos(promedioT);
				}
				else if(anio.split(" ").length!=2)
				{
					actual.setPromedioAnualVotos(0);
				}

				//Rating
				if(actual.getIMDBData().getImdbRating().equals("N/A"))
				{
					actual.setRatingIMBD(0.0);
				}
				else if(!actual.getIMDBData().getImdbRating().equals("N/A"))
				{
					double rating = Double.parseDouble(actual.getIMDBData().getImdbRating());
					actual.setRatingIMBD(rating);
				}
				//Pais
				actual.setPais(actual.getIMDBData().getCountry());

				misPeliculas.agregarElementoFinal(actual);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return existe;
	}

	private ListaEncadenada2<VOGeneroPelicula> agregarGeneros(String string)
	{
		ListaEncadenada2 retorno = new ListaEncadenada2<VOGeneroPelicula>();
		String[] generos = string.split(" ,");
		for(int i = 0; i<generos.length; i++)
		{
			VOGeneroPelicula actual = new VOGeneroPelicula();
			actual.setNombre(generos[i]);
			retorno.agregarElementoFinal(actual);
		}
		return retorno;
	}
	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		misUsuarios = new ListaEncadenada2<VOUsuario>();
		try(BufferedReader br = new BufferedReader(new FileReader(rutaRatings));) {
			String line = br.readLine();
			while (line!=null) {
				VORating VoRating = new VORating();
				if (line.startsWith("u"))line = br.readLine();
				String ratings[]=line.split(",");
				long UsuarioID = Long.parseLong(ratings[0]);
				long PeliculaID = Long.parseLong(ratings[1]);
				double rating = Double.parseDouble(ratings[2]);
				long timestamp = Long.parseLong(ratings[3]);
				VoRating.setIdUsuario(UsuarioID);
				VoRating.setMovieid(PeliculaID);
				VoRating.setRating(rating);
				VoRating.setTimestamp(timestamp);
				misRatings.agregarElementoPrincipio(VoRating);
				line=br.readLine();
				return true;
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	public VOUsuarioPelicula nuevoUsuario(String line)
	{
		String datos[] = line.split(",");
		VOUsuarioPelicula user = new VOUsuarioPelicula();

		int idUser = Integer.parseInt(datos[0]);
		user.setIdUsuario(idUser);
		System.out.println(user.getIdUsuario());
		return user;
	}

	public int cantidadComas(String linea)
	{
		int cantidad=0;
		char lineas[]=linea.toCharArray();
		for (int i = 0; i < lineas.length; i++) {
			if (lineas[i]==',') {
				cantidad++;
			}
		}
		return cantidad;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		try (BufferedReader br = new BufferedReader(new FileReader(rutaTags));) {
			String line = br.readLine();
			while (line!=null) {
				VOTag VoTag = new VOTag();
				if (line.startsWith("u"))line = br.readLine();
				if (line.contains("\"")) {
					int cantidad = cantidadComas(line);
					if (cantidad==5) {
						String pegar[] = line.split(",");
						String tag = pegar[2]+pegar[3]+pegar[4];
						long timestamp = Long.parseLong(pegar[5]);
						VoTag.setTag(tag);
						VoTag.setTimestamp(timestamp);
						misTags.agregarElementoPrincipio(VoTag);
						line = br.readLine();
					}
					if (cantidad==6) {
						String pegar[] = line.split(",");
						String tag = pegar[2]+pegar[3]+pegar[4]+pegar[5];
						long timestamp = Long.parseLong(pegar[6]);
						VoTag.setTag(tag);
						VoTag.setTimestamp(timestamp);
						misTags.agregarElementoPrincipio(VoTag);
						line = br.readLine();
					}
				}
				String partsOfTags [] = line.split(",");
				long usuarioID = Long.parseLong(partsOfTags[0]);
				int PeliculaID = Integer.parseInt(partsOfTags[1]);
				VoTag.setIdPelicula(PeliculaID);
				String tag = partsOfTags[2];
				long timestamp = Long.parseLong(partsOfTags[3]);
				VoTag.setTag(tag);
				VoTag.setTimestamp(timestamp);
				VoTag.setIdUsuario(usuarioID);
				misTags.agregarElementoPrincipio(VoTag);
				//----------------------------------------////Lista de ratings por peliculas//----------------------------------------//
				line = br.readLine();
			}
			String action = "Ha cargado los tags";
			if (misTags.darNumeroElementos()!=0) {
				return true;
			}
		}
		catch (Exception e) {
			e.getMessage();
		}
		return false;
	}



	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {
		if(idUsuario!=null&&ruta==null)
		{
			Solicitud solicitud = new Solicitud(idUsuario);
			solicitud.setIdUsuario(idUsuario);
			solicitudes.insert(solicitud);
		}
		else if(idUsuario==null&&ruta!=null)
		{
			Solicitud solicitud = new Solicitud(ruta);
			solicitud.setRuta(ruta);
			solicitudes.insert(solicitud);
		}
	}

	@Override
	public void generarRespuestasRecomendaciones() {
		JsonArray array = new JsonArray();
		JsonObject objs= new JsonObject();

		Double prediccion = 0D;
		for (int i = 0; i < 10; i++) {
			JsonObject obj = new JsonObject();
			Solicitud recomendacion = solicitudes.max();

			if(recomendacion!=null)
			{
				RojoNegro<Double, VOPelicula> arbol= new RojoNegro<Double,VOPelicula>();
				VOPelicula[] pelis = new VOPelicula[5];
				String u = ""+recomendacion.getIdUsuario();
				int use=0;
				try {
					int us = Integer.parseInt(u);
					use = us;
					obj.addProperty("user_id", u);
					VOUsuario user = buscarUsuarioID(us);

					for(VOPelicula peli : misPeliculas){

						String moveID = peli.getMovieID(); 

						prediccion = prediccion(use, moveID);
						if(prediccion !=0)
							arbol.put(prediccion, buscarPelicula(moveID));

						pelis[Integer.parseInt(moveID)%5]= buscarPelicula(moveID);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				if(recomendacion != null)
				{
					JsonArray array3= new JsonArray();
					for (int j = 0; j < 5; j++) {
						JsonObject ob = new JsonObject();
						ob.addProperty("item_id", pelis[j].getMovieID());
						prediccion = prediccion(use, pelis[j].getMovieID());
						ob.addProperty("p_rating",prediccion);
						array3.add(ob);
					}
					obj.add("recommendations", array3);
					array3.add(obj);
					JsonObject obj1= new JsonObject();
					obj1.add("response_users", array3);
					objs.add("response", obj1);
				}
				else if(recomendacion == null)
				{
					JsonArray array2= new JsonArray();
					for (int j = 0; j < pelis.length; j++) {
						JsonObject ob = new JsonObject();
						ob.addProperty("item_id",pelis[j].getMovieID());
						prediccion = prediccion(use,pelis[j].getMovieID());
						ob.addProperty("p_rating",prediccion);
						array2.add(ob);
					}
					obj.add("recommendations", array2);
					array.add(obj);
					JsonObject obj1= new JsonObject();
					obj1.add("response_users", array);
					objs.add("response", obj1);
				}
			}
		}
		try {
			Date fecha = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_kk_mm_ss");
			String date = sdf.format(fecha);
			String ruta ="data/R2/Recomendaciones_"+date+".json";
			File f = new File(ruta);

			FileWriter file;

			file = new FileWriter(f);
			file.write(objs.getAsString());
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		ListaEncadenada2<VOPelicula> retorno = new ListaEncadenada2<VOPelicula>();
		ListaEncadenada2<VOPelicula> auxRetorno = new ListaEncadenada2<VOPelicula>();
		if (misGeneros3.contains(genero.getNombre())) 
		{
			RojoNegro<Date,VOPelicula> arbolGenre = misGeneros3.get(genero.getNombre());
			ListaEncadenada2<VOPelicula> arbolInorden = arbolGenre.inOrderList(retorno);
			for (VOPelicula voPelicula : arbolInorden) 
			{
				Date fechas = voPelicula.getFechaLanzamineto();
				if (fechas.getTime()>=fechaInicial.getTime()&&fechas.getTime()<=fechaFinal.getTime()) 
				{
					auxRetorno.agregarElementoPrincipio(voPelicula);
				}
			}
		}
		return auxRetorno;
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) {
		VORating nuevo = new VORating();
		double error = (rating - prediccion(idUsuario, ""+idPelicula));
		if(error<0)
		{
			error *= (-1);
		}
		nuevo.setIdUsuario(idUsuario);
		nuevo.setMovieid(idPelicula);
		nuevo.setRating(rating);
		nuevo.setError(error);

		VOPelicula peli = buscarPelicula(""+idPelicula);
		peli.setPromedioRating(rating);
	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) 
	{
		ListaEncadenada2<VOUsuarioPelicula> lista=new ListaEncadenada2<>();
		Iterator iter = misUsuarios.iterator();
		while(iter.hasNext())
		{
			VOUsuarioPelicula a=(VOUsuarioPelicula) iter.next();
			if(idUsuario==a.getIdUsuario())
			{
				lista.agregarElementoFinal(a);

			}
		}
		return lista;
	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		tags();
		int conformeCounter=0; int inconformeCounter=0; int neutralCounter=0; int noClasificadoCounter=0;
		misTags.cambiarActualAPrimero();
		misTagsSegmento.cambiarActualAPrimero();
		for (VOUsuario voUsuario : misUsuarios) {
			ListaEncadenada2<VOTag> aux = new ListaEncadenada2<>();
			aux = darTagsUsuario(voUsuario.getIdUsuario());
			if (aux.isEmpty()==true)continue;
			if (aux.darNumeroElementos()!=0) 
			{
				aux.cambiarActualAPrimero();
				for (VOTag voTag : aux) {
					if (voTag.getConformismo().startsWith("con"))
					{
						conformeCounter++;
					}
					else if (voTag.getConformismo().startsWith("in"))
					{
						inconformeCounter++;
					}
					else if (voTag.getConformismo().startsWith("neu"))
					{
						neutralCounter++;
					}
					else if (voTag.getConformismo().startsWith("no-"))
					{
						noClasificadoCounter++;
					}
				}
				int arreglo [] = {conformeCounter,inconformeCounter,neutralCounter,noClasificadoCounter};
				QuickSort a = new QuickSort();
				a.mergeSort(arreglo);
				if (arreglo[3]==conformeCounter) 
				{
					voUsuario.setConformismo(CONFORME);
				}
				else if (arreglo[3]==inconformeCounter) 
				{
					voUsuario.setConformismo(INCONFORME);
				}
				else if (arreglo[3]==neutralCounter) 
				{
					voUsuario.setConformismo(NEUTRAL);
				}
				else if (arreglo[3]==noClasificadoCounter) 
				{
					voUsuario.setConformismo(NOCLASIFICADO);
				}
			}
		}
	}

	@Override
	public void ordenarPeliculasPorAnho() throws ParseException {
		SimpleDateFormat k= new SimpleDateFormat("dd MMMM yyyy");
		arbolOrdenAnio = new RedBlack<Integer, SeparateChainingHashST<String, RedBlack<Double,Lista2<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>>>>();
		for(VOPelicula peli : misPeliculas){
			int anio=0;
			Date fecha = k.parse(peli.getIMDBData().getReleased());
			int id = Integer.parseInt(peli.getMovieID());
			if(peli.getIMDBData().getYear().length()>4) anio = Integer.parseInt(peli.getIMDBData().getYear()); 
			else if(peli.getIMDBData().getYear().equals("N/A"))
				continue;
			else{ 				
				String temp[]=peli.getIMDBData().getYear().split("-");
				anio=Integer.parseInt(temp[0]);
			}
			String gen = peli.getIMDBData().getGenre();
			VOPelicula nPeli = new VOPelicula();
			nPeli.setNombre(peli.getIMDBData().getTitle());
			nPeli.setRatingIMBD(Double.parseDouble(peli.getIMDBData().getImdbRating()));
			nPeli.setVotostotales(Integer.parseInt(peli.getIMDBData().getImdbVotes()));
			nPeli.setFechaLanzamineto(fecha);
			if(arbolOrdenAnio.contains(anio))
			{
				if(arbolOrdenAnio.get(anio).contains(gen))
				{
					if(arbolOrdenAnio.get(anio).get(gen).contains(nPeli.getRatingIMBD()))
					{
						if(arbolOrdenAnio.get(anio).get(gen).get(nPeli.getRatingIMBD())!=null) arbolOrdenAnio.get(anio).get(gen).get(nPeli.getRatingIMBD()).add(nPeli, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
						else;
					}
					else
					{
						Lista2<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new Lista2<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
						list.add(nPeli, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
						arbolOrdenAnio.get(anio).get(gen).put(nPeli.getRatingIMBD(), list);
					}
				}
				else
				{
					Lista2<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new Lista2<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
					list.add(nPeli, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
					RedBlack arbol = new RedBlack<Double,Lista2>();
					arbol.put(nPeli.getRatingIMBD(), list);
					arbolOrdenAnio.get(anio).put(gen, arbol);
				}
			}
			else
			{
				Lista2<VOPelicula, SeparateChainingHashST<Integer, VOUsuarioPelicula>>list=new Lista2<VOPelicula,SeparateChainingHashST<Integer, VOUsuarioPelicula>>();
				list.add(nPeli, new SeparateChainingHashST<Integer,VOUsuarioPelicula>(sizeUsersSR()));
				RojoNegro<Double, Lista2> three=new RojoNegro<Double,Lista2>();
				three.put(nPeli.getRatingIMBD(), list);
				SeparateChainingHashST table=new SeparateChainingHashST<>(sizeUsersSR());
				table.put(gen, three);
				arbolOrdenAnio.put(anio, table);

			}
		}

	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {

		VOReporteSegmento resp = new VOReporteSegmento();

		//Variables para la parte 1
		Double promedioError = 0.0;
		Double numerador = 0.0;
		Double denominador = 0.0;

		//Variables para la parte 2

		//Lista a agregar al reporte (Lista verdadera)
		ListaEncadenada2<VOGeneroPelicula> verdad = new ListaEncadenada2<>();

		//Lista con los generos 
		ListaEncadenada2<VOGeneroPelicula> resp2 = new ListaEncadenada2<VOGeneroPelicula>();



		//Lista con los usuarios del segmento
		ListaEncadenada2<VOUsuario> usuarios = darUsuariosPorSegmento(segmento);

		//Falta inicializar los VOUsuarioPelicula
		ListaEncadenada2<VOUsuarioPelicula> usuariosP = new ListaEncadenada2<>();

		//Lista Con los VOUsuarioPelicula ya filtrados
		ListaEncadenada2<VOUsuarioPelicula> usuP = new ListaEncadenada2<>();

		//Lista con los ids de los usuarios del segmento
		ListaEncadenada2<Long> ids = new ListaEncadenada2<>();
		for(VOUsuario usu: usuarios)
		{
			ids.agregarElementoPrincipio(usu.getIdUsuario());
		}
		for(Long id: ids )
		{
			for(VOUsuarioPelicula up: usuariosP)
			{
				if((int)up.getIdUsuario()==id)
				{
					usuP.agregarElementoFinal(up);
				}
			}
		}
		//Ya est� la lista (usuP) con los VOUsuarioPelicula filtrados

		//Recorro cada VOUsuarioPelicula
		for(VOUsuarioPelicula usuuP: usuP)
		{
			//Obtener el error promedio, parte 1 del req
			numerador += usuuP.getErrorRating(); 
			if(usuuP.getErrorRating() != 0){denominador++;}

			//Parte 2 del req, 5 generos con m�s cantidad de ratings
			String nomPeliActual = usuuP.getNombrepelicula();
			VOPelicula peliActual = buscarPelicula(nomPeliActual);

			//Generos asociados a la pelicula que ha sido rateada (generos que han sido rateados por ende)
			ILista<VOGeneroPelicula> generosActu = peliActual.getGenerosAsociados();

			//Voy a hacer un recorrido de todos los generos asociados a cada pelicula de cada VOUsuarioPelicula
			for(VOGeneroPelicula geneActu:generosActu)
			{
				//Agrego cada genero, o agrego +1 a cantidad de veces que ha sido rateado 
				agregarGeneroAux(resp2, geneActu);
			}


		}
		VOGeneroPelicula[] arreglo  = new VOGeneroPelicula[resp2.darNumeroElementos()];
		for (int i = 0; i < resp2.darNumeroElementos(); i++) 
			arreglo[i] = resp2.darElemento(i);
		Arrays.sort(arreglo);
		promedioError = numerador/denominador;
		resp.setErrorPromedio(promedioError);
		for(int i=5;i>0;i--)
		{
			VOGeneroPelicula actual = resp2.darElemento(i);
			verdad.agregarElementoPrincipio(actual);
		}
		resp.setGenerosMasRatings((ILista<VOGeneroPelicula>) verdad);

		return resp;

	}

	public ListaEncadenada2<VOUsuario> darUsuariosPorSegmento(String pSegmento)
	{
		ListaEncadenada2<VOUsuario> retornar = new ListaEncadenada2<>();
		misUsuarios.cambiarActualAPrimero();
		for (int i = 0; i < misUsuarios.darNumeroElementos(); i++) {
			if (misUsuarios.darElementoPosicionActual().getConformismo().equalsIgnoreCase(pSegmento)) {
				retornar.agregarElementoPrincipio(misUsuarios.darElementoPosicionActual());
			}
			misUsuarios.avanzarSiguientePosicion();
		}
		return retornar;
	}

	public VOPelicula buscarPelicula(String t)
	{
		for(VOPelicula resp: misPeliculas)
		{
			if(resp.getNombre().equals(t))
			{
				return resp;
			}
		}
		return null;
	}

	public void agregarGeneroAux(ListaEncadenada2<VOGeneroPelicula> lista, VOGeneroPelicula genero)
	{
		Iterator<VOGeneroPelicula> iter = lista.iterator();
		while(iter.hasNext())
		{
			VOGeneroPelicula actual = iter.next();
			if(actual.getNombre().equals(genero.getNombre()))
			{
				actual.setNombre("");
			}
		}
		lista.agregarElementoFinal(genero);
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial,
			Date fechaFinal) {
		return peliculasGenero(genero, fechaInicial, fechaFinal);
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) 
	{
		MaxPQ<VOPelicula> pq = new MaxPQ<>();
		ListaEncadenada2<VOPelicula> resp = new ListaEncadenada2<>();
		for(VOPelicula actual: misPeliculas)
		{
			int anioInicial = actual.getFechaLanzamineto().getYear();
			String temp[] = actual.getIMDBData().getYear().split("-");
			anioInicial = Integer.parseInt(temp[0]);
			int promedioActual = actual.getVotostotales()/(2016-anioInicial);
			actual.setPromedioAnualVotos(promedioActual);
			actual.setPrioridad(actual.getPromedioAnualVotos()*actual.getRatingIMBD());
			pq.insert(actual);
		}
		for(int i=n;i>0;i--)
		{
			resp.agregarElementoPrincipio(pq.max());
		}
		return resp;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		return misPeliculas;
	}
	//-------------------------------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------AUXILIAR-------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------------------
	public void tags() 
	{
		for (VOTag voTag : misTags) {
			voTag.setConformismo(darSegmento(voTag.getTag()));
		}
	}

	public String darSegmento(String pTag)
	{
		for (VOTagSegmento voTagSegmento : misTagsSegmento) {
			if (voTagSegmento.getTag().contains(pTag)) {
				return voTagSegmento.getSegmento();
			}
		}
		return "no-clasificado";
	}

	public ListaEncadenada2<VOTag> darTagsUsuario(long id) 
	{
		ListaEncadenada2<VOTag> retorno = new ListaEncadenada2<VOTag>();
		misTags.cambiarActualAPrimero();
		for (int i = 0; i < misTags.darNumeroElementos(); i++) 
		{
			VOTag a = misTags.darElementoPosicionActual();
			if (a.getIdUsuario()==id) 
			{
				retorno.agregarElementoPrincipio(a);
			}
			misTags.avanzarSiguientePosicion();
		}
		return retorno;
	}

	private VOUsuario buscarUsuarioID(long id){
		Iterator<VOUsuario> iter = misUsuarios.iterator();
		while(iter.hasNext()){
			VOUsuario us = iter.next();
			if(us.getIdUsuario() == id)
				return us;
		}
		return null;
	}

	public double prediccion(long idUsuario, String movieID ){
		Double f1 =0.0;
		Double f2 =0.0;
		Iterator iter=misUsuarios.iterator();
		while(iter.hasNext()){
			VOUsuarioPelicula p = (VOUsuarioPelicula) iter.next();
			if(p.getIdUsuario()==idUsuario){
				Double a = similitud(movieID, p.getNombrepelicula());
				f1 += a*p.getRatingUsuario();
				f2 += a;
			}
		}
		return f1/f2;
	}

	public double similitud(String p1 , String p2){
		Double f1= 0.0;
		Double f2 = 0.0;
		Double f3 = 0.0;
		Iterator iter = misUsuarios.iterator();
		Iterator iter2 = misUsuarios.iterator();
		while(iter.hasNext())
		{
			VOUsuarioPelicula u= (VOUsuarioPelicula)iter.next();
			if(u.getNombrepelicula().equals(p1))
			{
				Integer idU=u.getIdUsuario();
				while(iter2.hasNext())
				{
					VOUsuarioPelicula u2= (VOUsuarioPelicula)iter.next();
					if(u2.getNombrepelicula().equals(p2)&&u2.getRatingUsuario()==idU)
					{
						double a=u.getRatingUsuario();
						double a2=u2.getRatingUsuario();
						f1+=a2*a;
						f2+=a*a;
						f3+=a2*a2;
					}
				}

			}

		}
		double r1= Math.sqrt(f2);
		double r2 = Math.sqrt(3);
		return f1/(r1*r2);
	}

}
