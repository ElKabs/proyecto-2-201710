package logic;


public class Solicitud implements Comparable<Solicitud> {
	private Integer idUsuario = 0; 
	private Long timeStamp = 0L;
	private Integer numTags = 0;
	private Integer numRatings = 0;
	private String ruta = "";
	public Solicitud(Integer idUsuario, Long timeStamp, Integer numTags, Integer numRatings) 
	{
		idUsuario = idUsuario;
		timeStamp = timeStamp;
		numTags = numTags;
		numRatings = numRatings;
	}
	public Solicitud(Integer idUsuario, Integer prioridad) {
		idUsuario = idUsuario;
		prioridad = prioridad;
	} 
	public Solicitud(Integer idUsuario2) {
		idUsuario = idUsuario2;
	}
	public Solicitud(String ruta)
	{
		ruta = ruta;
	}
	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public Integer getNumTags() {
		return numTags;
	}
	public void setNumTags(Integer numTags) {
		this.numTags = numTags;
	}
	public Integer getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(Integer numRatings) {
		this.numRatings = numRatings;
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int compareTo(Solicitud a)
	{
		if(ruta=="")
		{
			return -1;
		}
		else if(a.getTimeStamp()>timeStamp)
		{
			return -1;
		}
		else if(a.getTimeStamp()<timeStamp)
		{
			return 1;
		}
		else if(a.getTimeStamp()==timeStamp)
		{
			if(a.getNumRatings()>numRatings)
			{
				return -1;
			}
			else if(a.getNumRatings()<numRatings)
			{
				return 1;
			}
			else if(a.getNumRatings()==numRatings)
			{
				if(a.getNumTags()>numTags)
				{
					return -1;
				}
				else if(a.getNumTags()<numTags)
				{
					return 1;
				}
				else if(a.getNumTags()==numTags)
				{
					return 0;
				}
			}
		}
		return 0;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
}
