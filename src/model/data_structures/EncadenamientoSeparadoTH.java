package model.data_structures;

import java.util.Iterator;

import model.data_structures.*;
import model.data_structures.ListaLlaveValorSecuencial.Nodo;;


public class EncadenamientoSeparadoTH <K,T>{

	ListaLlaveValorSecuencial<K, T>[] arreglo;
	
	int tamanio;
	
	public EncadenamientoSeparadoTH(int a){
		arreglo = (ListaLlaveValorSecuencial<K, T>[]) new Object[a];
		for (int i = 0; i < arreglo.length; i++) {
			arreglo[i] = (ListaLlaveValorSecuencial<K, T>) new Object();
		}
	}
	public int size(){
		int size=0;
		for(ListaLlaveValorSecuencial<K,T> a : arreglo)
			size += a.size();
		return size;
	}
	
	public boolean estaVacia(){
		boolean ret = false;
		for(ListaLlaveValorSecuencial<K,T> a : arreglo){
			if(a.size() != 0)
				ret = false;
		}
		return ret;
	}
	
	public boolean tieneLlave(K key){
		int pos = hash(key);
		ListaLlaveValorSecuencial<K,T> lista = arreglo[pos];
		if(lista.tieneLlave(key))return true;
		return false;
	}
	
	private int hash(K key) {
        return (key.hashCode() & 0x7fffffff) % arreglo.length;
    }
	
	public T darValor(K key){
		int pos = hash(key);
		T res = arreglo[pos].darValor(key);
		if(res != null)	return res;
		else	return null;
	}
	
	public void insertar(K key, T valor){
		int pos = hash(key);
		if(valor  == null){
			ListaLlaveValorSecuencial<K, T> lista  = arreglo[pos];
			if(lista.tieneLlave(key)){
				int pos1 = lista.darPosConKey(key);
				lista.remove(pos1);
			}
		}
		else {
			ListaLlaveValorSecuencial<K, T> lista = arreglo[pos];
			Nodo<K, T> nodo = lista.primera;
			while(nodo.siguiente != null){//Por si acaso REVISAR
				if(nodo.key == key)
					nodo.setValor(valor);
				nodo = nodo.siguiente;
			}
		}
		rehash();
	}
	
	public ListaEncadenada2<K> listaDeLlaves(){
		ListaEncadenada2<K> listaDeLlaves = new ListaEncadenada2<>();
		for(ListaLlaveValorSecuencial<K, T> a : arreglo){
			Iterator<K> ite=a.iterator();
			while(ite.hasNext()){
				listaDeLlaves.agregarElementoFinal(ite.next());
			}
		}
		return listaDeLlaves;
	}
	
	public Iterator<K> iterator() {
		return listaDeLlaves().iterator();
	}
	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////
	
	public void rehash(){
		if(arreglo.length/tamanio <= 0.4){
			ListaLlaveValorSecuencial<K, T>[] temp = arreglo;
			int tamanio1 = arreglo.length;
			arreglo = (ListaLlaveValorSecuencial<K, T>[]) new Object[tamanio1*2];
			for(int i = 0 ; i < arreglo.length ; i++)
				arreglo[i] = (ListaLlaveValorSecuencial<K, T>) new Object();
			for (int i = 0; i < temp.length; i++) 
				arreglo[i] = temp[i];
			
		}
		
	}
	public int[] darLongitudListas(){
		int[] retorno = new int[arreglo.length];
		
		for (int i = 0; i < arreglo.length; i++) {
			retorno[i] = arreglo[i].size();
			tamanio += arreglo[i].size();
		}
		return retorno;
	}
	
	
}
