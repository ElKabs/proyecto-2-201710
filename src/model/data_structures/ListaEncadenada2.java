package model.data_structures;

import java.util.Iterator;

import vos.VOPelicula;


public class ListaEncadenada2<T> implements API.ILista<T>{

	private Node<T> primero;
	private Node<T> actual;
	private int listSize;
	
	public ListaEncadenada2()
	{
		primero = new Node<T>(null);
		actual = primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
			{
			private Node<T> act=null;
	
			public boolean hasNext() 
			{
				if(act==null)return primero.getItem()!=null;
				else return act.getNext() != null;
			}
	
			public T next() 
			{
				if(act==null)
				{
					act=primero;
					if(act==null)return null;
					else return act.getItem();
				}
				else
				{
					act = act.getNext();
					return act.getItem();
				}
	
			}
		};
}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			Node<T> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setNext(new Node<T>(elem));
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}
	
	public void agregarElementoPrincipio(T elem)
	{
		Node<T> newNode = new Node(elem);
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			newNode.setNext(primero);
			primero=newNode;
		}
		listSize++;
	}
	
	public T quitarElementoPrincipio()
	{
		if(primero == null)
		{
			System.out.println("No hay elementos");
		}
		T elem = primero.getItem();
		Node<T> siguientePrimero = primero.getNext();
		primero.setItem(null);
		primero = siguientePrimero;
		listSize--;
		return elem;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		Node<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean retrocederPosicionAnterior() 
	{
		Node<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}
	
	public void cambiarActualAPrimero()
	{
		actual=primero;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void cambiar(int pos, T elem)
	{
		int i = 0;
		if(primero != null)
		{
			actual = primero;
			while(i<pos)
			{
				actual = actual.getNext();
				i++;
			}
			actual.setItem(elem);
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
}
