package model.data_structures;

import java.util.Iterator;



public class ListaLlaveValorSecuencial <K,T>{

	int size = size();
	Nodo<K,T> primera  = null;
	Nodo<K,T> ultima  = null;
	ListaLlaveValorSecuencial<K, T> lista;
	
 
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	
	public Iterator<K> iterator() {
		Iterator<K> it = new MiIterator<K>(primera);
		return it;
	}
	
	public class MiIterator <K> implements Iterator <K>  {

		Nodo<K,T> actual;

		public MiIterator (Nodo<K,T> PprimerElemento){
			actual = PprimerElemento; 
		}

		@Override
		public boolean hasNext() {

			if(size() == 0){
				return false;
			}

			if(actual == null)
				return true;
			return actual.siguiente != null;
		}

		@Override
		public K next() {
			if(actual == null)
				actual = (Nodo<K,T>) primera;
			else
				actual = actual.siguiente;
			
			if(actual != null)
				return actual.key;
			else
				return null;
		}

	}

	//////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////

	public ListaLlaveValorSecuencial(){size = 0;}


	public int size() {
		return size;
	}

	public boolean estaVacia(){
		return size == 0;
	}
	public boolean tieneLlave(K key){
		Nodo<K, T> actual = primera;
		while(actual.siguiente != null){
			if(actual.key == key)
				return true;
		}
		return false;
	}
	public T darValor(K key){
		Nodo<K, T> actual = primera;
		while(actual.siguiente != null){
			if(actual.key == key)
				return actual.valor;
		}
		return null;
	}
	public void insertar(K key, T valor){
		Nodo<K, T> agregar = new Nodo<K,T>(key,valor);
		if(primera == null){
			primera = agregar;
			ultima = primera;
		}
		else{
			Nodo<K, T> a = ultima;
			ultima = agregar;
			a.siguiente = ultima;
		}
	}
	
	
	///////////////////////////////////////////////////////////////////
	//Metodos auxiliares
	///////////////////////////////////////////////////////////////////
	public int darPosConKey(K key){
		int cont = 0;
		Nodo<K, T> actual = primera;
		while(actual.siguiente != null){
			if(actual.key != key)
				cont++;
			else
				break;
		}
		return cont;
	}
	public void remove(int posicion){
		if(primera == null)
			throw new IndexOutOfBoundsException();

		else{
			Nodo<K,T> actual = primera;
			Nodo<K,T> anterior = null;

			if(posicion == 0)
				primera = actual.siguiente;

			else
			{
				while(actual != null  && posicion > 0)
				{
					anterior = actual;
					actual = actual.siguiente;
					posicion --;					
				}

				if(posicion > 0)
					throw new IndexOutOfBoundsException();

				else
				{
					if(actual != null)
						anterior.siguiente = actual.siguiente;

					else
						anterior.siguiente = null;


				}	
			}
		}
	}
	

	public static class Nodo <K,T>
	{
		K key;
		T valor;
		Nodo<K,T> siguiente;


		public Nodo(K elemento, T valor) {
			this.key = elemento;
			this.valor = valor;
		}


		public void setValor(T valor) {
			this.valor = valor;
		}
		

	}
}
