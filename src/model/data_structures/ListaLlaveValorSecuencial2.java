package model.data_structures;

import java.util.Iterator;

public class ListaLlaveValorSecuencial2<K extends Comparable<K>,V > implements IListaLlaveValorSecuencial<K, V> 
{

	private NodeDos<K,V> cabeza; 
	private NodeDos<K, V> actual; 
	private NodeDos<K, V>next; 
	private int listSize; 
	@Override
	public void ListaLlaveValorSecuencial() {
		cabeza = new NodeDos<>(); 
		actual = cabeza; 
		listSize = 0; 
	}
	public NodeDos<K, V> darPrimero(){return cabeza;}

	@Override
	public int darTamanio() {
		return listSize; 
	}

	@Override
	public boolean estaVacia() {
		if (listSize == 0){ return true;} 
		else return false; 
	}

	@Override
	public boolean tieneLlave(K llave) {
		NodeDos<K, V> actual = cabeza; 
		boolean respuesta = false; 
		while (actual.hasNext()){
			if (actual.getLlave().compareTo(llave) == 0 ) respuesta = true; 
			else actual = actual.getNext(); 

		}
		return respuesta;  
	}

	@Override
	public V darValor(K llave) {
		V respuesta = null; 
		if (tieneLlave(llave)){
			boolean encontrado = false; 
			NodeDos<K, V> actual = cabeza; 
			while (actual.hasNext() && encontrado != false ){
				if (actual.getLlave().equals(llave)){
					respuesta = actual.getValor(); 
				}
				else{
					actual = actual.getNext(); 
				}
			}
		}
		else { return null;}
		return respuesta; 
	}

	@Override
	public void insertar(K llave, V valor) {
		NodeDos<K, V> newNode = new NodeDos<>();
		newNode.setLlave(llave);newNode.setValor(valor);
		NodeDos<K, V> actual = cabeza;
		if (this.estaVacia()){cabeza = newNode;} 
		else{
			while(actual.hasNext()){
				if (actual.getLlave().compareTo(newNode.getLlave()) == 0){
					
						newNode.setNext(actual.getNext());
						actual.setNext(newNode);
						listSize++; 
					
				}
				else if (actual.getLlave().compareTo(newNode.getLlave())<0){
					newNode.setNext(actual.getNext());
					actual.setNext(newNode);
					listSize++; 
				}
				else{actual=actual.getNext();}
			}
		}
	}

	@Override
	public Iterable<K> llaves() {
		ListaEncadenada2<K> lista = new ListaEncadenada2<>(); NodeDos<K, V> actual = new NodeDos<>(); 
		while(cabeza.hasNext()){
			actual = cabeza; 
			lista.agregarElementoPrincipio(actual.getLlave());
			actual = actual.getNext(); 
		}
		return lista; 
	}
}


