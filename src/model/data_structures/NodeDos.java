package model.data_structures;

public class NodeDos<K,V> 
{
	private K llave;
	private V valor;

	private NodeDos<K,V> next;

	public NodeDos(K pllave, V pvalor)
	{
		next = null;
		this.llave= pllave; 
		this.valor=pvalor; 
	}


	public NodeDos() {
		// TODO Auto-generated constructor stub
	}

	public K getLlave() {
		return llave;
	}
	public V getValor() {
		return valor;
	}

	public void setLlave(K llave) {
		this.llave = llave;
	}
	public void setValor(V valor) {
		this.valor = valor;
	}

	public NodeDos<K,V> getNext() 
	{
		return next;
	}


	public void setNext(NodeDos<K,V> next) 
	{
		this.next = next;
	}
	public boolean hasNext(){
		if (this.next != null) return true; 
		else return false; 
	}


	

	
	
}
