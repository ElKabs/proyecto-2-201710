package model.data_structures;

import java.util.Comparator;

import API.ILista;
import model.data_structures.*;
import model.data_structures.ListaEncadenada2;
import vos.VOPelicula;

public class Ordenar<T> 
{
	public <T> void quick(API.ILista<T> retorno, int left, int right, Comparator<T> comparador)
	{
		if(left < right)
		{
			int k = left;
			int j = right;
			T pivot = retorno.darElemento((k + j) / 2);
			while(k <= j)
			{
				while(comparador.compare((T) retorno.darElemento(k), pivot)<0)
				{
					k++;
				}
				while(comparador.compare(pivot, (T) retorno.darElemento(j))<0)
				{
					j--;
				}
				if(k <= j)
				{
					T aux = (T) retorno.darElemento(k);
					((ListaEncadenada2<T>) retorno).cambiar(k, retorno.darElemento(j));
					((ListaEncadenada2<T>) retorno).cambiar(j, aux);
					k++;
					j--;
				}
			}
			quick(retorno, left, j, comparador);
			quick(retorno, k, right, comparador);
		}
	}
	
	public ListaEncadenada2<T> orden(ILista<T> jLista, Comparator<T> comparador)
	{
		ILista<T> retorno = jLista;
		quick(retorno, 0, jLista.darNumeroElementos()-1, comparador);
		return (ListaEncadenada2<T>) retorno;
	}
}
